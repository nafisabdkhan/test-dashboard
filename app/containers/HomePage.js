import { connect } from 'react-redux';
import Dashboard from '../components/Dashboard';

function mapStateToProps(state) {
  console.log(state);
  return {
    ...state,
    widgets: state.WidgetManager.widgets
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openWidgetManager: () => {
      dispatch({
        type: 'WIDGET_MANAGER_OPEN'
      });
    },
    addNewWidget: () => {
      dispatch({
        type: 'WIDGET_MANAGER_ADD'
      });
    },
    widgetFetchData: url => dispatch(fetchData(url))
  };
}

function itemsHasErrored(bool) {
  return {
    type: 'ITEMS_HAS_ERRORED',
    hasErrored: bool
  };
}
function itemsIsLoading(bool) {
  return {
    type: 'ITEMS_IS_LOADING',
    isLoading: bool
  };
}
function itemsFetchDataSuccess(data) {
  return {
    type: 'ITEMS_FETCH_DATA_SUCCESS',
    data
  };
}

function fetchData(url) {
  return dispatch => {
    dispatch(itemsIsLoading(true));
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(itemsIsLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(items => dispatch(itemsFetchDataSuccess(items)))
      .catch(() => dispatch(itemsHasErrored(true)));
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
