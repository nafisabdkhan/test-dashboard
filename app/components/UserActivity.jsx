// @flow
import React, { Component } from 'react';
import { Item, Progress, Grid, Header } from 'semantic-ui-react'

type Props = {};

export default class UserActivity extends Component<Props> {
    props: Props;

    render() {
      const props = { ...this.props }
      console.log(props);
        return (
          <Item>
            <Grid size='tiny' container stretched textAlign='center' verticalAlign='middle'>
              <Grid.Column width={2}>
                <Item.Image src='../resources/avatars/avatar.png' />
              </Grid.Column>
              <Grid.Column width={7}>
                <Header as='h5'>{props.name + " " + props.lastname}</Header>
              </Grid.Column>
              <Grid.Column width={5}>
                <Item.Description>
                  <Progress size="tiny" percent={props.percentage} success />
                </Item.Description>
              </Grid.Column>
              <Grid.Column width={2}>
                <Header as='h5'>{Math.round(props.percentage).toString() + '%'}</Header>
              </Grid.Column>
            </Grid>
          </Item>
        );
    }
}
