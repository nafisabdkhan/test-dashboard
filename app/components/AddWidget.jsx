// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Grid, Button,
     Icon, Item, Tab } from 'semantic-ui-react'

type Props = {
  isOpen: boolean,
  // isAvail: boolean,
  closeWidgetManager: () => void,
  // isWidgetAvail: () => void,
  addNewWidget: () => void
};

let props = {}

const panes = [
  { menuItem: 'My Widget', render: () => (
      <Tab.Pane>
        <Item.Group>
          <Item>
            <Item.Image size='tiny' src='../resources/widget.png' />

            <Item.Content>
              <Item.Header as='a'>Users worked more than required</Item.Header>
              <Item.Description>
                <Grid container stretched>
                  <Grid.Column width={10}>
                        Users who worked more or less than their minimum hours required in weekly     
                  </Grid.Column>
                  <Grid.Column width={6}>
                    <Grid.Row>
                      <Button
                        primary
                        onClick={props.addNewWidget}
                      >
                        <Icon name='add' /> Add Widget
                      </Button>
                    </Grid.Row>
                    <Grid.Row><br /></Grid.Row>
                    <Grid.Row>
                      <Button negative size='tiny'>
                        <Icon name='trash' /> Remove Widget
                      </Button>
                    </Grid.Row>
                  </Grid.Column>
                </Grid>
              </Item.Description>
              <Item.Extra>Additional Details</Item.Extra>
            </Item.Content>
          </Item>
        </Item.Group>
      </Tab.Pane>
    )
  },
  { menuItem: 'Widget Directory', render: () => (
      <Tab.Pane>
        <Item.Group>
          <Item>
            <Item.Image size='tiny' src='../resources/widget.png' />
            <Item.Content>
              <Item.Header as='a'>Users worked more than required</Item.Header>
              <Item.Description>
                <Grid container stretched>
                  <Grid.Column width={10}>
                        Users who worked more or less than their minimum hours required in weekly       
                  </Grid.Column>
                  <Grid.Column width={6}>
                    <Button primary>
                      <Icon name='add' /> Add Widget
                    </Button>
                  </Grid.Column>
                </Grid>
              </Item.Description>
              <Item.Extra>Additional Details</Item.Extra>
            </Item.Content>
          </Item>
        </Item.Group>
      </Tab.Pane>
    )
  }
]
  
  const TabbedPanel = () => (
    <Tab menu={{ fluid: true, vertical: true }} menuPosition='left' panes={panes} />
  )

export class AddWidget extends Component<Props> {
    props: Props;

    render() {
      props = { ...this.props }
      console.log(this.props);
        return (
          this.props.isOpen && 
          <Modal open closeIcon onClose={props.closeWidgetManager}>
            <Modal.Header>Add a widget</Modal.Header>
            <Modal.Content>
              <TabbedPanel />
            </Modal.Content>
            <Modal.Actions>
              <Button default
                onClick={props.closeWidgetManager}
              >
                <Icon name='close' /> Cancel
              </Button>
              <Button primary
                onClick={props.closeWidgetManager}
              >
                <Icon name='check circle outline' /> Save
              </Button>
            </Modal.Actions>
          </Modal>
        );
    }
}

function mapStateToProps(state) {
  return {
    ...state,
    isOpen: state.WidgetManager.on
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeWidgetManager: () => {
      dispatch({
        type: 'WIDGET_MANAGER_CLOSE'
      })
    },
    isWidgetAvail: () => {
      dispatch({
        type: 'WIDGET_MANAGER_AVAIL'
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddWidget)
