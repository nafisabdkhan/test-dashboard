// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Button, Icon, Form } from 'semantic-ui-react'

type Props = {};

class EditWidget extends Component<Props> {
    props: Props;

    render() {
      const props = { ...this.props }
      console.log(props);
      //props.openEdit()
        return (
          <Modal open={props.isOpen} closeIcon onClose={props.closeEdit}>
            <Modal.Header>Top highest perentage of Mobile Time Users</Modal.Header>
            <Modal.Content>
              <Form>
                <Form.Field>
                  <label>Number of Users</label>
                  <input type="number" min={1} max={5} defaultValue={props.number} onChange={ e => props.numberChanged(e.target.value) } />
                </Form.Field>
                <Form.Field>
                  <label>Ativity</label>
                  <Form.Group widths='equal'>
                    <Form.Field label='Highest' control='input' type='radio' name='htmlRadios' value='d' checked={props.order === 'd'}onChange={ e => props.orderChanged(e.target.value) } />
                    <Form.Field label='Lowest' control='input' type='radio' name='htmlRadios' value='a' checked={props.order === 'a'}
                    onChange={ e => props.orderChanged(e.target.value) } />
                  </Form.Group>
                </Form.Field>
                <Form.Field label='Time' control='select'>
                  <option value='Mobile Time'>Mobile Time</option>
                </Form.Field>
                <Form.Field label='Date' control='select'>
                  <option value='Weekly'>Weekly</option>
                </Form.Field>
              </Form>
            </Modal.Content>
            <Modal.Actions>
              <Button default onClick={props.closeEdit}>
                <Icon name='close' /> Cancel
              </Button>
              <Button primary onClick={() => props.onSave({ number: props.number, order: props.order }) }>
                <Icon name='check circle outline' /> Save
              </Button>
            </Modal.Actions>
          </Modal>
        );
    }
}

function mapStateToProps(state) {
  return {
    ...state,
    isOpen: state.EditWidget.on,
    number: state.EditWidget.number,
    order: state.EditWidget.order
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeEdit: () => {
      dispatch({
        type: 'EDIT_WIDGET_CLOSE'
      })
    },
    openEdit: () => {
      dispatch({
        type: 'EDIT_WIDGET_OPEN'
      })
    },
    numberChanged: val => {
      dispatch({
        type: 'EDIT_NUMBER',
        value: val
      })
    },
    orderChanged: val => {
      dispatch({
        type: 'EDIT_ORDER',
        value: val
      })
    },
    onSave: obj => {
      dispatch({
        type: 'EDIT_SAVE',
        obj: obj
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditWidget)
