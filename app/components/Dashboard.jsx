// @flow
import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
import {
  Icon, Container, Button, 
  Grid, Header, Dimmer,
  Loader
} from 'semantic-ui-react'

import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'

import { Responsive, WidthProvider } from 'react-grid-layout'
import Widget from './Widget'
import AddWidget from './AddWidget'

const ResponsiveGridLayout = WidthProvider(Responsive, { measureBeforeMount: true });

type Props = {
  openWidgetManager: () => void,
  widgets: Array
};

export default class Dashboard extends Component<Props> {
  props: Props;

  componentWillMount() {
    this.props.widgetFetchData('https://gist.githubusercontent.com/nunomluz/d01defb4b5cae3d40658d465e15640e2/raw/738bf5c86b9976d266871147992d4ed9bb4b6d14/data.json')
  }

  render() {
    const props = {...this.props}
    console.log(props);
    if(props.widgets === undefined) props.widgets = []
    if (props.itemsHasErrored) {
      return <p>Sorry! There was an error loading the items</p>
    }
    if (props.itemsIsLoading) {
        return <Dimmer active>
        <Loader />
      </Dimmer>
    }
    return (
      <Grid container>
        <Grid.Row>
          <Container>
            <Header as='h2' floated='left'>Team Dashboard</Header>
            <Button
              primary
              floated='right'
              onClick={this.props.openWidgetManager}
            >
              <Icon name="add" />Add Widget
            </Button>
          </Container> 
        </Grid.Row>
        <Grid.Row>
          <Container fluid>
            <ResponsiveGridLayout
              className="layout"
              breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
              cols={{lg: 3, md: 3, sm: 3, xs: 3, xxs: 2}}
              draggableHandle=".handle"
              isResizable={false}
              rowHeight={400}
            >
              {props.widgets.map((d, i) => <Widget number={5} order='a' data={props.data} key={i.toString()} data-grid={{x: (i % 3), y: 0, w: 1, h: 1}} />)}
            </ResponsiveGridLayout>
            <AddWidget 
              addNewWidget={props.addNewWidget} 
              isOpen={props.WidgetManager.on}
            />
          </Container>
        </Grid.Row>
      </Grid>
      
    );
  }
}
