// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    Grid,
    Segment, Header, Item, Dropdown
} from 'semantic-ui-react'

import UserActivity from './UserActivity'
import EditWidget from './EditWidget'

type Props = {};

class Widget extends Component<Props> {
    props: Props;

    render() {
        const props = {...this.props}
        console.log(props);
        // props.order = 'a'
        // props.number = 5
        return (
          <div {...this.props}>
            <Segment.Group size="tiny">
              <Segment size="tiny">
                <Grid size="tiny">
                  <Grid.Column className="handle" width={13}>
                    <Header as='h5' floated='left' style={{ cursor: "move" }}>User Activity</Header>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Dropdown icon='ellipsis vertical'>
                      <Dropdown.Menu>
                        <Dropdown.Item onClick={props.openEdit}>Edit Widget</Dropdown.Item>
                        <Dropdown.Item onClick={() => props.deleteWidget(props.key)}>Delete Widget</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                    <EditWidget
                      isOpen={props.EditWidget.on}
                      order={props.order}
                      number={props.number}
                    />
                  </Grid.Column>
                </Grid>
              </Segment>
              <Segment>
                <Item.Group>
                  {props.data.users.sort((a, b) => props.order === 'a' ? 
                  a.percentage > b.percentage : 
                  a.percentage < b.percentage).filter((d, i) => i < props.number).map(d => <UserActivity key={d.id} { ...d } />)}
                </Item.Group>
              </Segment>
            </Segment.Group>
          </div>
        );
    }
}

function mapStateToProps(state) {
  console.log(state);
  return state.EditWidget.save !== null ? {
    ...state,
    order: state.EditWidget.save.order,
    number: parseInt(state.EditWidget.save.number)
  } : {
    ...state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeEdit: () => {
      dispatch({
        type: 'EDIT_WIDGET_CLOSE'
      })
    },
    openEdit: () => {
      dispatch({
        type: 'EDIT_WIDGET_OPEN'
      })
    },
    deleteWidget: (id) => {
      dispatch({
        type: 'DELETE_WIDGET',
        id: id
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Widget)

