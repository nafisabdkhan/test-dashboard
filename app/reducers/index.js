// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { data, itemsHasErrored, itemsIsLoading } from './dashboard';
import WidgetManager from './WidgetManager';
import EditWidget from './EditWidget';

const rootReducer = combineReducers({
  data,
  itemsHasErrored,
  itemsIsLoading,
  WidgetManager,
  EditWidget,
  router
});

export default rootReducer;
