export function itemsHasErrored(state = false, action) {
  switch (action.type) {
    case 'ITEMS_HAS_ERRORED':
      return action.hasErrored;
    default:
      return state;
  }
}
export function itemsIsLoading(state = false, action) {
  switch (action.type) {
    case 'ITEMS_IS_LOADING':
      return action.isLoading;
    default:
      return state;
  }
}
export function data(state = [], action) {
  switch (action.type) {
    case 'ITEMS_FETCH_DATA_SUCCESS': {
      const dat = action.data;
      dat.users = action.data.users.map(d => {
        const dd = d;
        dd.percentage =
          action.data.weekly[
            Object.keys(action.data.weekly).filter(k => k === d.id)[0]
          ];
        return dd;
      });
      return dat;
    }
    default:
      return state;
  }
}
