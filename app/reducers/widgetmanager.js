const initialState = {
  on: false,
  avail: true,
  widgets: []
};

export default function WidgetManager(state = initialState, action) {
  switch (action.type) {
    case 'WIDGET_MANAGER_OPEN':
      return {
        ...state,
        on: true
      };
    case 'WIDGET_MANAGER_CLOSE':
      return {
        ...state,
        on: false
      };
    case 'WIDGET_MANAGER_AVAIL':
      return {
        ...state,
        avail: !state.avail
      };
    case 'WIDGET_MANAGER_ADD':
      return {
        ...state,
        on: false,
        widgets: (() => {
          state.widgets.push('user_activity');
          return state.widgets;
        })()
      };
    case 'DELETE_WIDGET': {
      console.log(state);
      const wd = state.widgets;
      wd.splice(parseInt(action.id, 10), 1);
      return {
        ...state,
        widgets: wd
      };
    }
    default:
      return state;
  }
}
