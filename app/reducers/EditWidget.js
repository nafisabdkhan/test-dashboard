const initialState = {
  on: false,
  order: 'a',
  number: 5,
  save: null
};

export default function EditWidget(state = initialState, action) {
  switch (action.type) {
    case 'EDIT_WIDGET_OPEN':
      return {
        ...state,
        on: true,
        save: null
      };
    case 'EDIT_WIDGET_CLOSE':
      return {
        ...state,
        on: false
      };
    case 'EDIT_WIDGET_SAVE':
      return {
        ...state,
        order: action.order,
        number: action.number
      };
    case 'EDIT_NUMBER': {
      return {
        ...state,
        number: action.value
      };
    }
    case 'EDIT_ORDER':
      return {
        ...state,
        order: action.value
      };
    case 'EDIT_SAVE': {
      console.log('aaa');
      return {
        ...state,
        save: action.obj,
        on: false
      };
    }
    default:
      return state;
  }
}
